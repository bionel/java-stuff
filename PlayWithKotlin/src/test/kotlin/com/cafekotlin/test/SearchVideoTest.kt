package com.cafekotlin.test

import com.cafekotlin.UtilResources
import com.cafekotlin.webpages.HomePage
import com.cafekotlin.webpages.ResultPage
import org.testng.Assert
import org.testng.annotations.Test

class SearchVideoTest() : TestBase() {

    @Test
    fun searchVideo() {
        val homePage = HomePage(driver!!)
        homePage.searchVideo(UtilResources.getProperties("nameVideo"))

        val resultPage = ResultPage(driver!!)
        Assert.assertTrue(resultPage.isPageOpened())

        resultPage.selectVideo(UtilResources.getProperties("selectVideo"))

        Assert.assertTrue(resultPage.playingVideo(UtilResources.getProperties("selectVideo"),
                UtilResources.getProperties("channel")))
    }

}