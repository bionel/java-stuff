package TestExamples

import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

@Stepwise
class CExpectWhereTest extends Specification {
    def "do third test"() {
        expect:
        "a*a equals b"
        a * a == b

        where:
        "a have the value"
        a = 2
        b = 4
    }

//    def "do fourth test"() {
//        expect:
//        "a*a equals b"
//        a * a == b
//
//        where:
//        "a have the value"
//        a  | b
//        2  | 4
//        4  | 16
//        10 | 100
//    }

//    @Unroll
//    def "an unroll of the fourth test"() {
//        expect:
//        "a*a equals b"
//        a * a == b
//
//        where:
//        "a have the value"
//        a  | b
//        2  | 4
//        4  | 16
//        10 | 100
//    }

}