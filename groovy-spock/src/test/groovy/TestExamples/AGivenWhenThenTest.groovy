package TestExamples

import spock.lang.Specification

class AGivenWhenThenTest extends Specification {

    def "do first test" ( ) {
        given:
        "a is empty list"
            def a = []

        when:
        "push one element to list"
        a << 'one element'

        then:
        "a contains the element"
        a.contains 'one element'
    }
}