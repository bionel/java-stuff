package TestExamples

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import utils.ExecutionProfile as EP

@Stepwise
class BWhenThenTest extends Specification {

    static env = System.properties['executionProfile'] ?: 'test'
    static profilePath = './src/test/resources/'
    static globalVariables = new EP().getGlobalsFromProperties("${profilePath}/${env}.properties")

    def setupSpec() {
        globalVariables.put('c', 4)
        globalVariables.otherVar = 'overridenStringValue'
        globalVariables.keyFromSpecSetup = 'valueFromSpecSetup'
        reportHeader "<h2>Test executed using the \'${env}\' profile data</h2>"
        println('ExecutionProfile in use:')
        globalVariables.each { k, v -> println("$k : $v - ${k.getClass()}")}

    }

    def "do the When-Then test"() {
        when:
        "a and b take values from globals"
        globalVariables.keyFromWhenThenTest = 'whateverValue'
        def a = globalVariables.a as int
        def b = globalVariables.b as int
        reportInfo """a has value ${a}"""
        reportInfo """b has value ${b}"""
        //println(System.properties['execution.profile'])

        then:
        "a square is b"
        a * a == b
    }

    def "do the Expect test"() {
        expect:
        "check if globals has new keys"
        globalVariables.contains(4)
        globalVariables.containsValue('overridenStringValue')
        globalVariables.containsKey('keyFromSpecSetup')
        globalVariables.containsKey('keyFromWhenThenTest')

    }
}