import groovyx.net.http.RESTClient
import org.apache.http.client.protocol.ClientContext
import org.apache.http.protocol.ExecutionContext

import static groovyx.net.http.ContentType.*

def httpReq(String baseUrl) {
    def request = new RESTClient(baseUrl)
    request.contentType = JSON
    request.headers.Accept = JSON
    request.headers.Application = JSON
    //request.setHeaders(['Accept' : JSON, 'Application' : JSON])
    return request
}

//handle request
req = httpReq('https://gitlab.com')

println('\n-------- Request properties: ------------')
req.properties.each { println "${it}" }

println('\n-------- Request headers: ------------')
req.headers.each { k, v -> println "${k} : ${v}" }

//handle response
resp = req.get([path: '/api/v4/users/bionel/projects'])

println('\n-------- Response properties: ------------')
resp.properties.each { println "${it}" }

assert resp.status == 200
assert resp.success
assert resp.contentType == JSON.toString()

println('\n-------- Response status/content: ------------')
println(resp.status)
println(resp.contentType)

println('\n-------- Response Headers: ------------')
resp.headers.each { println "${it.name} : ${it.value}" }

println('\n-------- Response Data: ------------')
resp.data.each { obj ->
    println()
    obj.each { k, v -> println("$k : $v") }
}

//println('\n-------- Response Context: ------------')
//println(resp.context.getAttribute(ExecutionContext.HTTP_RESPONSE))
//println(resp.context.getAttribute(ClientContext.???))


//####################################################//
//import static groovyx.net.http.HttpBuilder.configure

//def httpReq = configure {
//    request.uri = 'https://gitlab.com'
//    request.uri.path = '/api/v4/users/bionel/projects'
//}

//def httpResp = httpReq.get()

//httpResp.each { obj ->
//    obj.each { k, v -> println("$k : $v")}
//    println()
//}

//
//def result = httpReq.get {
//    request.uri.path = '/api/v4/users/bionel/projects'
//}
//
//result.each { obj ->
//    obj.each { k, v -> println("$k : $v")}
//    println()
//}