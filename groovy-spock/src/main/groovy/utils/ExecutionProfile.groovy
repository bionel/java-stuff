package utils

class ExecutionProfile {

    def getGlobalsFromProperties(String propFile) {
        def PropertiesMap = new Properties()
        def PropertiesFile = new File(propFile)
        PropertiesFile.withInputStream {
            PropertiesMap.load(it)
        }
        return PropertiesMap
    }

}
