import utils.ExecutionProfile

def testProfile = new ExecutionProfile()

myGlobals = testProfile.getGlobalsFromProperties('../resources/test.properties')

println(myGlobals)

println(myGlobals.a)
println(myGlobals.b)
println(myGlobals.otherVar)

def env = System.getenv()

env.each{
    println it
}