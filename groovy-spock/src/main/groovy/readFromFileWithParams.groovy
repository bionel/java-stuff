fileContents = new File('../resources/sourceFile').text
println(fileContents)

PARAM1 = 'firstValueHere'
PARAM2 = 'secondValueHere'

def engine = new groovy.text.SimpleTemplateEngine()
def binding=["PARAM1": PARAM1, "PARAM2": PARAM2]
def template = engine.createTemplate(fileContents).make(binding)

println('Substitute to:')
println(template.toString())